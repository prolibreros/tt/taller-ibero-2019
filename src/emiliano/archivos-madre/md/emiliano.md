
Dosier
# Conceptos y percepción: un análisis de la experiencia humana

## Introducción:

El presente dosier tiene como objetivo fundamental el llevar a cabo la contextualización de los términos propios en los que se desarrolla la discusión filosófica entre conceptualistas y no-conceptualistas. Si bien se trata de un preámbulo motivado por el interés en llevar a cabo la exposición de un caso en defensa de la primera de estas posturas, la selección de textos expuesta a continuación pretende ejercer como un menú de lectura variado, que capacite a cualquier lector con las herramientas necesaria para introducirse en el debate en cuestión.

### I.	La disyuntiva kantiana entre conceptos e intuiciones:

La irrupción del pensamiento kantiano en el marco de la discusión moderna entre racionalistas y empiristas dejó una huella determinante en el proceso de configuración de la epistemología como la conocemos en la actualidad. El motivo detrás de esto puede encontrarse en la adopción de una actitud conciliadora por parte de Kant, materializada en su esfuerzo por sintetizar los elementos pertinentes de cada postura en un mismo sistema. Quizás, uno de los puntos de la obra kantiana en donde mejor puede apreciarse el sentido de semejante actitud se ubique en la introducción general al capítulo de la Lógica trascendental, en la Crítica de la razón pura. Me refiero a aquel pasaje paradigmático en donde el filósofo de Königsberg manifiesta que “los pensamientos sin contenido son vacíos y las intuiciones sin conceptos son ciegas”.

### II.	La traducción del problema en términos de espontaneidad y receptividad:

En la contemporaneidad, al intentar rastrear la herencia de aquel planteamiento nos dirigimos irremediablemente hacia las teorías conceptualistas en filosofía analítica. En concreto, el libro _Mente y mundo_ del filósofo sudafricano John McDowell, se presenta como una entrada determinante a esta línea de pensamiento. Para los fines aquí perseguidos, hemos apelado a un fragmento que se despliega en torno a la necesidad de un cierto tipo de interpretación respecto a la disyuntiva kantiana mencionada en el apartado anterior, una interpretación centrada en la analogía entre las dualidades concepto-intuición y espontaneidad-receptividad. Pues es precisamente con base en esta segunda caracterización, que resulta factible llevar a cabo la expansión del marco de facultades posiblemente atribuibles a cada una de estas categorías. 
La idea de fondo, en apego a la dinámica de subsunción tan presente a lo largo de la primera _Crítica_, apunta a que la sensibilidad pueda ser identificada como un mecanismo conformado por un conjunto de operaciones inherentes a la espontaneidad, es decir, a las actividades de corte conceptual.

### III. Motivaciones no-conceptualistas:

En la búsqueda de lecturas que permitan acceder a la causa no conceptualista, el artículo _Kantian non-conceptualism_ del filósofo canadiense Robert Hanna resulta de gran utilidad. En el fragmento de este texto aquí recuperado, se aprecian cuando menos un par de aportaciones sustanciales. 
Primero, la delimitción de la postura no-conceptualista como aquella evocada a la defensa de la tesis que afirma las siguientes dos premisas: a) “el contenido representacional no está ni única ni exclusivamente determinado por nuestras capacidades conceptuales”  y b) “por lo menos algunos contenidos están tanto única como exclusivamente determinados por capacidades no-conceptuales, de manera que pueden ser compartidos por humanos y animales no-humanos por igual” . Al reparar en las implicaciones de la tesis nos percatamos de que para Hanna, el asunto en juego en el debate entre conceptualistas y no-conceptualistas se concentra fundamentalmente en el concepto de “contenido representacional”. Asimismo, puede observarse la introducción de un componente biológico al debate, ilustrado en la distinción entre humanos y animales no-humanos, como los dos géneros de sujeto sobre los que se cimienta la investigación.
La segunda aportación de relevancia por parte de Hanna, es la demarcación de dos razones por las que podría resultar pertinente adoptar una perspectiva no-conceptualista. La primera de estas es el interés en ponderar “una forma de realismo perceptual directo”, con lo cual, indirectamente, se deposita la problemática etiqueta de metafísica sobre el conceptualismo.

### IV.	La unidad de la apercepción como respuesta a Hanna:

El cuarto trabajo recuperado para el presente dosier, es el artículo _A Conceptualist Reply to Hanna’s Kantian Non-Conceptualism_ de Brady Bowman. Aquí, como el título mismo lo indica, con lo que nos encuentramos es con una suerte de respuesta conceptualista a los argumentos expuestos por Hanna, fundanda en la demostración de su discordancia con los objetivos marcados por Kant en la _Analítica trascendental_. En términos más específicos, el eje del fragmento seleccionado, parte de la identificación de que “dos consecuencias que se siguen de la interpretación de Hanna […] resultan fundamentalmente incompatibles con el espíritu y esfuerzo de la filosofía crítica de Kant”. 

