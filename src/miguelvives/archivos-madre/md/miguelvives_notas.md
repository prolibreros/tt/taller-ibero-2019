Tuñón Oviedo, Alberto Hidalgo. “Materialismo Filosófico.” Eikasia. Revista de filosofía, no. 2 (Enero 2006): 2. http://www.revistadefilosofia.org/MATERIALISMOFILOSOFICOesp.pdf. {frances}

Velasco, Jesús Martínez. “El Problema Mente-Cerebro: Sus Orígenes Cartesianos.” Contrastes. Revista Interdisciplinar De Filosofía 1 (1996): 201.http://www.revistas.uma.es/index.php/contrastes/article/view/1874/1818. {frances}

Kull, Kalevi. “On Semiosis, Umwelt, and Semiosphere.” Semiotica 120 (1998): 299–310. http://www.zbi.ee/~kalevi/jesphohp.htm. {frances}

Von Bertalanffy, Ludwig. “The Mind-Body Problem: A New View.” Pyshosomatic Medicine 26, no. 1 (1964): 43. http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.521.7045&rep=rep1&type=pdf. {frances}

Kuri, Ivonne, y Julio Muñoz Rubio. “El Darwinismo Neural y Los Orígenes De La 
Conciencia Humana: Una Crítica Desde La Dialéctica.” Scielo 19, no. 37 (Enero 2017). 
http://www.scielo.org.mx/scielo.php?script=sci_arttext&pid=S1665-13242017000100
170. {frances}
