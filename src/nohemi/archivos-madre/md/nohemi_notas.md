 Por ejemplo, En 2015, Daphne fue secuestrada y violada en Veracruz por cuatro jóvenes acaudalados conocidos como _Los Porkys_. La noticia y su desarrollo están publicados en Excelsior. El artículo puede consultarse [en línea] (https://www.excelsior.com.mx/nacional/2017/03/29/1140391)

Por mencionar algunos: el caso de la joven de Atzcapotzalco que fue violada por cuatro policías el 12 de agosto de 2019; el feminicidio de Lesvy en manos de su novio el 3 de mayo de 2017; el caso de Daphne, quien fue secuestrada y violada por cuatro jóvenes acaudalados conocidos como Los Porkys en 2015.

Rita Segato, _La escritura en el cuerpo de las mujeres asesinadas en Ciudad Juárez_ (Buenos Aires: Editorial Tinta Limón, 2013), 19-21.

Este concepto no será abordado de manera especial en el dosier, por lo que puede limitarse a entiéndase por patriarcado la estructura de dominación masculina y masculinizada en la que el control de las esferas de la vida, privadas y públicas, pertenece a los hombres.

Rita Segato, _La guerra contra las mujeres_ (Madrid: Editorial Traficantes de sueños, 2016), 39.

Rita Segato, _Las nuevas formas de guerra y el cuerpo de las mujeres_ (Puebla: Editorial Pez en el árbol/Tinta Limón, 2014), 22-23.

Mariana Berlanga, _Una mirada al feminicidio_ (México: Editorial Ítaca/Universidad Autónoma de la Ciudad de México, 2018), 68-69.

Mariana Berlanga, _Una mirada al feminicidio_, 98-99.

