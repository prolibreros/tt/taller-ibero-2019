Pecas: 2019.11.09-7e451d5
 Ruby: 2.6.4-p104
 Host: x86_64-pc-linux-gnu

Software bajo licencia GPLv3+: <https://gnu.org/licenses/gpl.html>.
Documentación: <http://pecas.cliteratu.re/>.

------------------------------------------------------------------

# pc-pandog
$ ruby /home/ted/.pecas/epub/automata/../../base-files/pandog/pandog.rb -i /home/ted/Repositorios/taller-ibero/src/nohemi/archivos-madre/md/nohemi.md -o /home/ted/Repositorios/taller-ibero/src/nohemi/epub-automata/.nohemi.xhtml

# pc-creator
$ ruby /home/ted/.pecas/epub/automata/../creator/creator.rb -o epub       

# pc-divider
$ ruby /home/ted/.pecas/epub/automata/../divider/divider.rb -f /home/ted/Repositorios/taller-ibero/src/nohemi/epub-automata/.nohemi.xhtml -d epub/OPS/xhtml -s epub/OPS/css/styles.css -i 3 

# pc-notes
$ ruby /home/ted/.pecas/epub/automata/../notes/notes.rb -n /home/ted/Repositorios/taller-ibero/src/nohemi/archivos-madre/md/nohemi_notas.md -d epub/OPS/xhtml -s epub/OPS/css/styles.css -i 3  

# pc-recreator
$ ruby /home/ted/.pecas/epub/automata/../recreator/recreator.rb -d epub -y /home/ted/Repositorios/taller-ibero/src/nohemi/epub-automata/meta-data.yaml 

# pc-changer
$ ruby /home/ted/.pecas/epub/automata/../changer/changer.rb -e /home/ted/Repositorios/taller-ibero/src/nohemi/epub-automata/epub-f26fe726.epub --version 3.0.0

# pc-analytics
$ ruby /home/ted/.pecas/epub/automata/../../base-files/analytics/analytics.rb -f /home/ted/Repositorios/taller-ibero/src/nohemi/epub-automata/epub-f26fe726.epub --json 

# epubcheck 4.0.2
$ java -jar /home/ted/.pecas/epub/automata/../../src/alien/epubcheck/4-0-2/epubcheck.jar epub-f26fe726.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove"
         date="2016-11-29"
         name="epubcheck"
         release="4.0.2">
     <date>2019-11-20T13:46:23-06:00</date>
     <repInfo uri="epub-f26fe726.epub">
        <created>2019-11-20T13:46:15Z</created>
        <lastModified>2019-11-20T13:46:16Z</lastModified>
        <format>application/epub+zip</format>
        <version>3.0.1</version>
        <status>Well-formed</status>
        <mimeType>application/epub+zip</mimeType>
        <properties>
           <property>
              <name>CharacterCount</name>
              <values arity="Scalar" type="Long">
                 <value>17586</value>
              </values>
           </property>
           <property>
              <name>Language</name>
              <values arity="Scalar" type="String">
                 <value>es</value>
              </values>
           </property>
           <property>
              <name>Info</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Identifier</name>
                    <values arity="Scalar" type="String">
                       <value>1_0_0-f26fe7262ba34f95a382f6314c7fb816</value>
                    </values>
                 </property>
                 <property>
                    <name>CreationDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2019-11-20T13:46:15Z</value>
                    </values>
                 </property>
                 <property>
                    <name>ModDate</name>
                    <values arity="Scalar" type="Date">
                       <value>2019-11-20T13:46:16Z</value>
                    </values>
                 </property>
                 <property>
                    <name>Title</name>
                    <values arity="Scalar" type="String">
                       <value>Argot político</value>
                    </values>
                 </property>
                 <property>
                    <name>Creator</name>
                    <values arity="Scalar" type="String">
                       <value>González García, Nohemí Raquel</value>
                    </values>
                 </property>
                 <property>
                    <name>Publisher</name>
                    <values arity="Scalar" type="String">
                       <value>Programando LIBREros</value>
                    </values>
                 </property>
                 <property>
                    <name>Subject</name>
                    <values arity="Scalar" type="String">
                       <value>filosofía</value>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>Fonts</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Regular</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Italic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya Bold</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
                 <property>
                    <name>Font</name>
                    <values arity="List" type="Property">
                       <property>
                          <name>FontName</name>
                          <values arity="Scalar" type="String">
                             <value>Alegreya BoldItalic</value>
                          </values>
                       </property>
                       <property>
                          <name>FontFile</name>
                          <values arity="Scalar" type="Boolean">
                             <value>false</value>
                          </values>
                       </property>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>References</name>
              <values arity="List" type="Property">
                 <property>
                    <name>Reference</name>
                    <values arity="Scalar" type="String">
                       <value>http://www.feministas.org/IMG/pdf/rita_segato_.pdf</value>
                    </values>
                 </property>
              </values>
           </property>
           <property>
              <name>MediaTypes</name>
              <values arity="Array" type="String">
                 <value>application/x-dtbncx+xml</value>
                 <value>application/xhtml+xml</value>
                 <value>text/css</value>
                 <value>application/vnd.ms-opentype</value>
              </values>
           </property>
        </properties>
     </repInfo>
  </jhove>

# epubcheck 3.0.1
$ java -jar /home/ted/.pecas/epub/automata/../../src/alien/epubcheck/3-0-1/epubcheck.jar epub-f26fe726_3-0-0.epub -out log.xml -q

  <?xml version="1.0" encoding="UTF-8"?>
  <jhove xmlns="http://hul.harvard.edu/ois/xml/ns/jhove" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="epubcheck" release="3.0.1" date="2013-05-10">
   <date>2019-11-20T13:46:24-06:00</date>
   <repInfo uri="epub-f26fe726_3-0-0.epub">
    <created>2019-11-20T13:46:15Z</created>
    <lastModified>2019-11-20T13:46:16Z</lastModified>
    <format>application/epub+zip</format>
    <version>3.0</version>
    <status>Well-formed</status>
    <mimeType>application/epub+zip</mimeType>
    <properties>
     <property><name>CharacterCount</name><values arity="Scalar" type="Long"><value>17586</value></values></property>
     <property><name>Language</name><values arity="Scalar" type="String"><value>es</value></values></property>
     <property><name>Info</name><values arity="List" type="Property">
      <property><name>Identifier</name><values arity="Scalar" type="String"><value>1_0_0-f26fe7262ba34f95a382f6314c7fb816</value></values></property>
      <property><name>CreationDate</name><values arity="Scalar" type="Date"><value>2019-11-20T13:46:15Z</value></values></property>
      <property><name>ModDate</name><values arity="Scalar" type="Date"><value>2019-11-20T13:46:16Z</value></values></property>
      <property><name>Title</name><values arity="Scalar" type="String"><value>Argot político</value></values></property>
      <property><name>Creator</name><values arity="Scalar" type="String"><value>González García, Nohemí Raquel</value></values></property>
      <property><name>Publisher</name><values arity="Scalar" type="String"><value>Programando LIBREros</value></values></property>
     </values></property>
     <property><name>Fonts</name><values arity="List" type="Property">
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Regular</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Italic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya Bold</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
      <property><name>Font</name><values arity="List" type="Property">
       <property><name>FontName</name><values arity="Scalar" type="String"><value>Alegreya BoldItalic</value></values></property>
       <property><name>FontFile</name><values arity="Scalar" type="Boolean"><value>true</value></values></property>
      </values></property>
     </values></property>
     <property><name>References</name><values arity="List" type="Property">
      <property><name>Reference</name><values arity="Scalar" type="String"><value>http://www.feministas.org/IMG/pdf/rita_segato_.pdf</value></values></property>
      <property><name>Reference</name><values arity="Scalar" type="String"><value>https://www.researchgate.net/publication/287830519_Las_nuevas_formas_de_la_guerra_y_el_cuerpo_de_las_mujeres</value></values></property>
      <property><name>Reference</name><values arity="Scalar" type="String"><value>https://www.traficantes.net/sites/default/files/pdfs/map45_segato_web.pdf</value></values></property>
     </values></property>
    </properties>
   </repInfo>
  </jhove>

# ace
$ ace -o logs/ace epub-f26fe726.epub
=> Ace no pudo ser localizado, el EPUB no puede ser verificado…

# kindlegen
$ kindlegen epub-f26fe726.epub

  *************************************************************
   Amazon kindlegen(Linux) V2.9 build 1028-0897292 
   A command line e-book compiler 
   Copyright Amazon.com and its Affiliates 2014 
  *************************************************************
  
  Info(prcgen):I1047: Added metadata dc:Title        "Argot político"
  Info(prcgen):I1047: Added metadata dc:Creator      "González García, Nohemí Raquel"
  Info(prcgen):I1047: Added metadata dc:Publisher    "Ibero"
  Info(prcgen):I1047: Added metadata fixed-layout    "false"
  Info(prcgen):I1047: Added metadata dc:Subject      "filosofía"
  Info(prcgen):I1047: Added metadata dc:Description  "[Edipo]consiguió dar sentido. Pero, curiosamente, esto no lo salvó, ni a él ni a Tebas, de su destino trágico [...] supongamos, que se ha descifrado el enigma, que sabemos lo que quiere decir. Sin embargo, al igual que el héroe trágico, nos sumergimos más y más en un destin
  Info(prcgen):I1002: Parsing files  0000004
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-count' in file: /tmp/mobi-SAIbuH/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-gap' in file: /tmp/mobi-SAIbuH/OPS/css/styles.css
  Warning(htmlprocessor):W28001: CSS style specified in content is not supported by Kindle readers. Removing the CSS property: 'column-rule' in file: /tmp/mobi-SAIbuH/OPS/css/styles.css
  Info(prcgen):I1015: Building PRC file
  Info(prcgen):I1006: Resolving hyperlinks
  Warning(prcgen):W14016: Cover not specified
  Info(pagemap):I8000: No Page map found in the book
  Info(prcgen):I1045: Computing UNICODE ranges used in the book
  Info(prcgen):I1046: Found UNICODE range: Basic Latin [20..7E]
  Info(prcgen):I1046: Found UNICODE range: Latin-1 Supplement [A0..FF]
  Info(prcgen):I1046: Found UNICODE range: General Punctuation - Windows 1252 [2013..2014]
  Info(prcgen):I1017: Building PRC file, record count:   0000006
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  52.09%
  Info(prcgen):I1040: The document identifier is: "Argot_politico"
  Info(prcgen):I1041: The file format version is V6
  Info(prcgen):I1031: Saving PRC file
  Info(prcgen):I1033: PRC built with WARNINGS!
  Info(prcgen):I1016: Building enhanced PRC file
  Info(prcgen):I1007: Resolving mediaidlinks
  Info(prcgen):I1011: Writing mediaidlinks
  Info(prcgen):I1009: Resolving guide items
  Info(prcgen):I1017: Building PRC file, record count:   0000010
  Info(prcgen):I1039: Final stats - text compressed to (in % of original size):  49.43%
  Info(prcgen):I1041: The file format version is V8
  Info(prcgen):I1032: PRC built successfully
  Info(prcgen):I15000:  Approximate Standard Mobi Deliverable file size :   0000025KB
  Info(prcgen):I15001:  Approximate KF8 Deliverable file size :   0000666KB
=>Info(prcgen):I1037: Mobi file built with WARNINGS!
  
